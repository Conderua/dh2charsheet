﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataStructures;
using DataStructures.Characteristics;

namespace DataStructuresTests
{
    [TestClass]
    public class CharacteristicTests
    {
        [TestMethod]
        public void WSCreationTest()
        {
            WeaponSkill WS=  new WeaponSkill(40);
        }
        [TestMethod]
        public void WSAdvanceValueTest()
        {
            WeaponSkill WS = new WeaponSkill(40);
            int WSValue = WS.Value;
            WS.AdvanceLevel=Characteristic.AdvanceLevels.Expert;
            Assert.IsTrue(WS.Value==WSValue+25);
        }
        [TestMethod]
        public void WSAdvanceBonusTest()
        {
            WeaponSkill WS = new WeaponSkill(40);
            int WSBonus = WS.Bonus;
            if(WSBonus!=4)
                Assert.Fail();
            WS.AdvanceLevel = Characteristic.AdvanceLevels.Expert;
            Assert.IsTrue(WS.Bonus==6);//(40+25)/10
        }
        [TestMethod]
        [ExpectedException(typeof (ArgumentOutOfRangeException))]
        public void WSLessThanZeroValue()
        {
            WeaponSkill WS = new WeaponSkill(-1);
        }
        [TestMethod]
        public void WSAdvanceXPCostTest()
        {
            //Related aptitudes - WS and Offence
            WeaponSkill WS = new WeaponSkill(40);
            WS.AdvanceLevel=Characteristic.AdvanceLevels.Expert;
            ReadOnlyCollection<Aptitude> twoAptitudes = new ReadOnlyCollection<Aptitude>(new List<Aptitude>() { Aptitude.WeaponSkill, Aptitude.Offence,Aptitude.Agility,Aptitude.Fellowship});
            ReadOnlyCollection<Aptitude> oneAptitude = new ReadOnlyCollection<Aptitude>(new List<Aptitude>() { Aptitude.Knowledge, Aptitude.Offence, Aptitude.Agility, Aptitude.Fellowship });
            ReadOnlyCollection<Aptitude> noAptitudes = new ReadOnlyCollection<Aptitude>(new List<Aptitude>() { Aptitude.Leadership, Aptitude.Intelligence, Aptitude.Agility, Aptitude.Fellowship });
            if(WS.XPSpent(twoAptitudes)!= 100 + 250 + 500 + 750 + 1250)
                Assert.Fail();
            if (WS.XPSpent(oneAptitude) != 250 + 500 + 750 + 1000 + 1500)
                Assert.Fail();
            if (WS.XPSpent(noAptitudes) != 500 + 750 + 1000 + 1500 + 2500)
                Assert.Fail();
        }
    }
}

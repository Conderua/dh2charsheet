﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace DataStructures
{
    public class Charsheet
    {
        public string CharacterName { get; set; }
        public string PlayerName { get; set; }

        public ReadOnlyCollection<Aptitude> Aptitudes => new ReadOnlyCollection<Aptitude>(aptitudes.ToList());

        private HashSet<Aptitude> aptitudes;
        //TODO: Characteristics, skills, talents&traits, aptitudes, armor,weapons, gear, psy powers, etc :)
    }
}

﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace DataStructures.Characteristics
{
    public abstract class Characteristic
    {
        public Characteristic(int baseValue)
        {
            this.BaseValue = baseValue;
        }

        #region INFO

        public abstract string Name { get; } //e.g. Weapon skill, Willpower, etc
        public abstract string ShortName { get; } //e.g. WS, WP, etc
        public abstract string Description { get; } //Just copy from the rulebook

        #endregion

        #region ADVANCE

        protected abstract ReadOnlyCollection<Aptitude> neededAptitudes { get; }

        /// <summary>
        /// List of characteristic advance levels. Core rulebook p. 80
        /// </summary>
        public enum AdvanceLevels
        {
            None = 0,
            Simple = 1,
            Intermediate = 2,
            Trained = 3,
            Proficient = 4,
            Expert = 5
        }

        /// <summary>
        /// Each characteristic grows by 5 with every advance. (Core rulebook p. 80). This is a multiplyer to count summary characteristic grow with current advance
        /// </summary>
        protected int AdvanceMultiplyer
        {
            get
            {
                switch (AdvanceLevel)
                {
                    case AdvanceLevels.None:
                        return 0;
                    case AdvanceLevels.Simple:
                        return 1;
                    case AdvanceLevels.Intermediate:
                        return 2;
                    case AdvanceLevels.Trained:
                        return 3;
                    case AdvanceLevels.Proficient:
                        return 4;
                    case AdvanceLevels.Expert:
                        return 5;
                    default:
                        throw new InvalidOperationException(
                            Resources.CharacteristicAdvanceMultiplyerWrongAdvanceLevelExceptionText);
                            //this means that advance level has unknown value.
                }
            }
        }

        /// <summary>
        /// Level of characteristic advance (number of dots in charlist)
        /// </summary>
        protected AdvanceLevels advanceLevel;

        public AdvanceLevels AdvanceLevel
        {
            get { return advanceLevel; }
            set
            {
                if (this is Influence)
                    throw new InvalidOperationException("Influence can not be advanced!");
                //TODO: Transfer eror messages to the resource file
                advanceLevel = value;
                Changed?.Invoke(this, Value);
            }
        }

        /// <summary>
        /// How many aptitudes matches for this skill. Core rulebook p.80
        /// </summary>
        /// <param name="aptitudes">List of the character's aptitudes</param>
        /// <returns>Number of the matching aptitudes(0 or 1 or 2)</returns>
        protected int MatchingAptitudes(ReadOnlyCollection<Aptitude> aptitudes)
        {
            return neededAptitudes.Intersect(aptitudes).Count();
        }

        /// <summary>
        /// XP cost of characteristic advance
        /// </summary>
        /// <param name="aptitudes">List of the character's aptitudes</param>
        /// <returns>XP cost of characteristic advance</returns>
        public int XPSpent(ReadOnlyCollection<Aptitude> aptitudes)
        {
            if (this is Influence)
                return 0;
            var matchingAptitudes = MatchingAptitudes(aptitudes);
            if (matchingAptitudes > 2)
                throw new InvalidOperationException("Too many aptitudes!");
                    //TODO: transfer error message to the resource file
            //Core rulebook p. 80

            #region NO_APTITUDES

            if (matchingAptitudes == 0)
            {
                switch (AdvanceLevel)
                {
                    case AdvanceLevels.None:
                        return 0;
                    case AdvanceLevels.Simple:
                        return 500;
                    case AdvanceLevels.Intermediate:
                        return 500 + 750;
                    case AdvanceLevels.Trained:
                        return 500 + 750 + 1000;
                    case AdvanceLevels.Proficient:
                        return 500 + 750 + 1000 + 1500;
                    case AdvanceLevels.Expert:
                        return 500 + 750 + 1000 + 1500 + 2500;
                    default:
                        throw new InvalidOperationException(
                            Resources.CharacteristicAdvanceMultiplyerWrongAdvanceLevelExceptionText);
                            //this means that advance level has unknown value.
                }
            }

            #endregion

            #region ONE_APTITUDE

            if (matchingAptitudes == 1)
            {
                switch (AdvanceLevel)
                {
                    case AdvanceLevels.None:
                        return 0;
                    case AdvanceLevels.Simple:
                        return 250;
                    case AdvanceLevels.Intermediate:
                        return 250 + 500;
                    case AdvanceLevels.Trained:
                        return 250 + 500 + 750;
                    case AdvanceLevels.Proficient:
                        return 250 + 500 + 750 + 1000;
                    case AdvanceLevels.Expert:
                        return 250 + 500 + 750 + 1000 + 1500;
                    default:
                        throw new InvalidOperationException(
                            Resources.CharacteristicAdvanceMultiplyerWrongAdvanceLevelExceptionText);
                            //this means that advance level has unknown value.
                }
            }

            #endregion

            #region TWO_APTITUDES

            if (matchingAptitudes == 2)
            {
                switch (AdvanceLevel)
                {
                    case AdvanceLevels.None:
                        return 0;
                    case AdvanceLevels.Simple:
                        return 100;
                    case AdvanceLevels.Intermediate:
                        return 100 + 250;
                    case AdvanceLevels.Trained:
                        return 100 + 250 + 500;
                    case AdvanceLevels.Proficient:
                        return 100 + 250 + 500 + 750;
                    case AdvanceLevels.Expert:
                        return 100 + 250 + 500 + 750 + 1250;
                    default:
                        throw new InvalidOperationException(
                            Resources.CharacteristicAdvanceMultiplyerWrongAdvanceLevelExceptionText);
                            //this means that advance level has unknown value.
                }
            }

            #endregion

            throw new InvalidOperationException($"Unknown eror while counting XP spent.");
                //TODO: Someone, write adequate eror messages, please!
        }

        #endregion

        #region VALUE

        /// <summary>
        /// Basic characteristic value, got after character creation
        /// </summary>
        /// <summary>
        /// Total characteristic value, including advances
        /// </summary>
        public int Value => this.BaseValue + (5*AdvanceMultiplyer);

        /// <summary>
        /// Characteristic bonus, including advances
        /// </summary>
        public int Bonus => this.Value/10;

        /// <summary>
        /// Basic characteristic value, got after character creation
        /// </summary>
        protected int baseValue;

        protected int BaseValue
        {
            get { return baseValue; }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Characteristic value can't be less, than 0");
                baseValue = value;
            }
        }

        #endregion

        #region DELEGATES

        public delegate void CharacteristicValue(object sender, int newValue);

        public event CharacteristicValue Changed;

        #endregion
    }
}
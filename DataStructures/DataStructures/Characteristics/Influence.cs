﻿using System.Collections.ObjectModel;

namespace DataStructures.Characteristics
{
    public class Influence:Characteristic
    {
        public override string Name { get; } = "Influence";
        public override string ShortName { get; } = "Inf";
        public override string Description { get; } = Resources.InfluenceDescription;
        protected override ReadOnlyCollection<Aptitude> neededAptitudes { get; }
        public Influence(int baseValue) : base(baseValue)
        {
        }
    }
}
﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace DataStructures.Characteristics
{
    public class WeaponSkill:Characteristic
    {
        public override string Name { get; } = "Weapon skill";
        public override string ShortName { get; } = "WS";
        public override string Description { get; } = Resources.WeaponSkillDescription;
        protected override ReadOnlyCollection<Aptitude> neededAptitudes { get; } = new ReadOnlyCollection<Aptitude>(new List<Aptitude>() {Aptitude.WeaponSkill,Aptitude.Offence});

        public WeaponSkill(int baseValue) : base(baseValue)
        {
        }
    }

    
}
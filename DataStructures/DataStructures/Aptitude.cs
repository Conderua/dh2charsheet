﻿namespace DataStructures
{
    public enum Aptitude
    {
        //Characteristic-based
        WeaponSkill,
        BallisticSkill,
        Strength,
        Toughness,
        Agility,
        Intelligence,
        Perception,
        Willpower,
        Fellowship,
        //
        Offence,
        Finesse,
        Defence,
        Psyker,
        Tech,
        Knowledge,
        Leadership,
        Fieldcraft,
        Social
    }
}